const crypto = require('crypto');
const https = require('https');
const path = require('path');
const fs = require('fs');

const VERBOSE = process.argv.indexOf('--silent') < 0;

/* Security algorithm */
const security = {
    algorithm: 'aes-256-cbc',
    init: 16,
    key: 32
};

/* List files or directories */
function ls(base = __dirname, mode = 'file') {
    return fs
        .readdirSync(path.resolve(base))
        .filter(name => {
            if (/^\./.test(name)) return false; // ignore hide files
            const isDir = fs.lstatSync(path.resolve(base, name)).isDirectory();
            return mode === 'dir' ? isDir : !isDir;
        })
        .map(name => [name, path.resolve(base, name)]);
}
const lsDirs = (base = __dirname) => ls(base, 'dir');
const lsFiles = (base = __dirname) => ls(base);

/* Watch folder */
function watchDir(dpath, callback, delay = 500) {
    /* Folder exists? */
    if (!fs.existsSync(dpath) || !fs.lstatSync(dpath).isDirectory()) {
        console.error(`Err: ${dpath} is not a folder.`);
        return;
    }
    /* Watch the changes */
    let timer;
    fs.watch(dpath, { recursive: true, persistent: true }, (e, fpath) => {
        const target = path.resolve(dpath, fpath);
        if (!fs.existsSync(target)) return;
        if (fs.lstatSync(target).isDirectory()) return;
        /* Callback */
        if (timer) clearTimeout(timer);
        timer = setTimeout(() => callback(dpath, fpath), delay);
    });
}

/*
 *  Main function
 *  Screeps synchronization
 */
function main() {
    /* Load auth.json */
    if (!fs.existsSync('./auth.json')) {
        if (VERBOSE) console.error('Err: please create auth.json file');
        return;
    }
    const auth = require('./auth.json');

    /* Setup - Security protocole */
    if (!auth.cryptoInit || !auth.cryptoKey) {
        if (!auth.email || !auth.pass) {
            if (VERBOSE) console.error('Err: wrong auth.json file');
            return;
        }
        /* Generate crypto sets */
        auth.cryptoInit = crypto.randomBytes(security.init).toString('hex');
        auth.cryptoKey = crypto.randomBytes(security.key).toString('hex');
        /* Encode the password */
        const cipher = crypto.createCipheriv(
            security.algorithm,
            Buffer.from(auth.cryptoKey, 'hex'),
            Buffer.from(auth.cryptoInit, 'hex')
        );
        auth.pass = cipher.update(
            `${auth.pass}${new Array(
                Math.max(0, security.init - auth.pass.length + 1)
            )
                .fill('')
                .join('\x00')}`,
            'utf-8',
            'hex'
        );
        auth.pass += cipher.final('hex');
        /* Update auth.json */
        fs.writeFileSync('./auth.json', JSON.stringify(auth, false, 4));
    }

    /* Decode */
    const decipher = crypto.createDecipheriv(
        security.algorithm,
        Buffer.from(auth.cryptoKey, 'hex'),
        Buffer.from(auth.cryptoInit, 'hex')
    );
    auth.pass = decipher.update(auth.pass, 'hex', 'utf-8');
    auth.pass += decipher.final('utf-8');
    auth.pass = auth.pass.replace(/\x00/g, '');

    /* Synchronize */
    function sync(branch, dpath, fpath) {
        if (VERBOSE) {
            console.log(`Update ${branch}/${fpath.replace(/\.js$/, '')}`);
        }
        /* Packing */
        const data = {
            branch,
            modules: lsFiles(path.resolve(dpath)).reduce(
                (accum, [name, fpath]) => {
                    accum[name.replace(/\.js$/, '')] = fs.readFileSync(
                        fpath,
                        'utf-8'
                    );
                    return accum;
                },
                {}
            )
        };
        /* Send to screeps server */
        const req = https.request({
            hostname: 'screeps.com',
            port: 443,
            path: '/api/user/code',
            method: 'POST',
            auth: `${auth.email}:${auth.pass}`,
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            }
        });
        req.write(JSON.stringify(data));
        req.end();
    }

    /* Scan the branchs */
    for (let [name, dpath] of lsDirs()) {
        /* Watch it */
        if (VERBOSE) console.log(`Watch '${name}' branch`);
        watchDir(dpath, (...args) => sync(name, ...args));
    }
}
main();
