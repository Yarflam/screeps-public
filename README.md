# Screeps

Screeps algorithm

## Install

```bash
git clone https://gitlab.com/Yarflam/screeps.git &&\
cd screeps &&\
cp auth.json.example auth.json &&\
nano auth.json
```

## Execute

```bash
node sync.js
```

Output example:

```text
Watch 'default' branch
Update default/main
Update default/role.harvester
...
```

## Links

-   [Survival playground](https://screeps.com/a/#!/sim/survival)
-   [API Reference](https://docs.screeps.com/api/)
-   [Documentation](https://docs.screeps.com/index.html)

## Author

-   Yarflam - _initial worker_
